### Log in
* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: str
  * password: str

* Response: Account information and a token
* Response shape (JSON):
  ```json
    {
      "account": {
        "key": type,
      },
      "token": str
    }
  ```

### Log out
* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
  ```json
  true
  ```

<!-- add get token request --> TODO

### Sign Up
* Endpoint path: /api/accounts
* Endpoint method: POST

* Request shape (JSON):
  ```json 
  {
      "first_name": str,
      "last_name": str,
      "email": str,
      "password": str,
      "password_validation": str,
      }
  ```

* Response: User account gets created
* Response shape (JSON):
  ```json 
  {
    "message": str
  }
  ```

<!-- ### Delete account
* Endpoint path: /api/accounts/{account_id}
* Endpoint method: DELETE
* Query parameters:
  * "account_id": get an account with that specific id, str

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
  ```json {
    "password": str,
  }
  ```

* Response: always true
* Response shape (JSON):
  ```json {
    true
    "message": str,
  }
  ``` -->
